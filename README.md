This is a guide is adapted from [Github](https://github.com/drduh/YubiKey-Guide/)

- [ECC Issues](##ECC-Issues)
- [Required software for Windows](##Required-software-for-Windows)
- [Required software for Linux](##Required-software-for-Linux)
- [Import Yubikey Public Keys (Widnows and Linux)](##Import-Yubikey-Public-Keys-(Widnows-and-Linux))
    -[Import and Trust root key](###Import-and-Trust-root-key)
- [SSH](##SSH)
- [Git](##Git)
- [Administration and KEY creation](#Administration-and-KEY-creation)

## ECC Issues

Concerning the generation of ECC keys instead of RSA keys the [Yubikey 4/5](https://support.yubico.com/support/solutions/articles/15000014219-yubikey-5-series-technical-manual#OpenPGPvbpoxl) only implements version 2.1 of the OpenPGP Card specification. ECDSA keys were added in [Version 3.0](https://bugzilla.mindrot.org/show_bug.cgi?id=2474). Also, [OpenSSH doesn’t support ECDSA](https://bugzilla.mindrot.org/show_bug.cgi?id=2474) on cards right aside from the people at [Fedora](https://fedoramagazine.org/fedora-28-better-smart-card-support-openssh/)

## Required software for Windows

The following are required: 
[Putty](https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html)
[GPG4Win](https://www.gpg4win.org/download.html). 
[Git-scm](https://git-scm.com/download/win)

For GPG4Win only GnuPG (core) and Kleopatra are required. Putty is required as only plink supports smart cards
For Git-SCM it is suggested you select Putty (not openSSH)
    - Git on windows with the yubkikey works with: CYGWIN, MINGW64 and Windows CMD.
    - It is recommended to choose the Windows terminal for Git (versus Mingw), this will make things easier later....

After installing these Windows will have multiple copies of ssh and gpg. Only the ones referenced in Git will matter for the purposes of using the Yubikey. For example your path my refer to other copies - you can either change your path or (more easily) just specify whats required for git.

Assuming you allready have a Yubikey try:

```
gpg --card-status
```
If you receive an error add the following to %APPDATA%\gnupg\scdaemon.conf

Windows can already have some virtual smartcard readers installed, like the one provided for Windows Hello. To ensure your YubiKey is the correct one used by scdaemon, you should add it to its configuration. You will need your device's full name. To find out what is your device's full name, plug your YubiKey, open the Device Manager, select "View->Show hidden devices". Go to the Software Devices list, you should see something like `Yubico YubiKey OTP+FIDO+CCID 0`. The name slightly differs according to the model. Thanks to [Scott Hanselman](https://www.hanselman.com/blog/HowToSetupSignedGitCommitsWithAYubiKeyNEOAndGPGAndKeybaseOnWindows.aspx) for sharing this information.

- Create or edit %APPDATA%/gnupg/scdaemon.conf, add `reader-port <your yubikey device's full name>`.
- In %APPDATA%/gnupg/gpg-agent.conf, add:
```
enable-ssh-support
enable-putty-support
```

**Reboot** and try again. It is normal for Windows to recognize the device as a NIST PIV smart card - This is related to using the Yubikey as a X509 certificate store as a part of the Windows CAPI store. 

- Open a command console, restart the agent:
```
> gpg-connect-agent killagent /bye
> gpg-connect-agent /bye
```
If you are sucessfull you will see somthing like [here](https://suchsecurity.com/gpg-and-ssh-with-yubikey-on-windows.html)

## Required software for Linux

**Linux** Install several packages required for the following steps (this example uses Ubuntu 16.04):

```
$ sudo apt-get update
$ sudo apt-get install -y \
     curl gnupg2 gnupg-agent \
     scdaemon pcscd \
     dirmngr \
     libyubikey-dev yubikey-personalization
```

Yubikey requires gpg2, you can either make all your calls with gpg2 or make a symbolic link

```
$ sudo mv /usr/bin/gpg /usr/bin/gpg1
$ sudo ln -s /usr/bin/gpg2 /usr/bin/gpg
```

If you need more recent versions of [yubikey-personalization](https://developers.yubico.com/yubikey-personalization/Releases/) and [yubikey-c](https://developers.yubico.com/yubico-c/Releases/)

If you need to set the U2F settings you need [ykman](https://developers.yubico.com/yubikey-manager/)

Note: Compiling from source gave erros on Ubuntu 16.04 wrt lib location and threading checks.... using repos is easier. 

```
$ wget https://developers.yubico.com/yubikey-personalization/Releases/ykpers-1.19.0.tar.gz
$ wget https://developers.yubico.com/yubikey-personalization/Releases/ykpers-1.19.0.tar.gz.sig
$ wget https://developers.yubico.com/yubico-c/Releases/libyubikey-1.13.tar.gz
$ wget https://developers.yubico.com/yubico-c/Releases/libyubikey-1.13.tar.gz.sig

$ gpg2 --verify ykpers-1.19.0.tar.gz.sig 
gpg: assuming signed data in `ykpers-1.19.0.tar.gz'
gpg: Signature made Tue 24 Apr 2018 04:29:05 AM EDT using RSA key ID B2168C0A
gpg: Good signature from "Klas Lindfors <klas@yubico.com>"
gpg: WARNING: This key is not certified with a trusted signature!
gpg:          There is no indication that the signature belongs to the owner.
Primary key fingerprint: 0A3B 0262 BCA1 7053 07D5  FF06 BCA0 0FD4 B216 8C0A

$ gpg2 --verify libyubikey-1.13.tar.gz.sig 
gpg: assuming signed data in `libyubikey-1.13.tar.gz'
gpg: Signature made Thu 05 Mar 2015 06:51:51 AM EST using RSA key ID B2168C0A
gpg: Good signature from "Klas Lindfors <klas@yubico.com>"
gpg: WARNING: This key is not certified with a trusted signature!
gpg:          There is no indication that the signature belongs to the owner.
Primary key fingerprint: 0A3B 0262 BCA1 7053 07D5  FF06 BCA0 0FD4 B216 8C0A
```
If you dont have the key you can import it
```
gpg2 --keyserver hkp://pgp.mit.edu --search B2168C0A
```
Verify the fingerprint from: [Here](https://developers.yubico.com/Software_Projects/Software_Signing.html)

Create a hardened configuration for GPG with the following options or by downloading my [recommended](https://github.com/drduh/config/blob/master/gpg.conf) version directly:

```
$ mkdir ~/.gnupg ; curl -Lfo ~/.gnupg/gpg.conf https://raw.githubusercontent.com/drduh/config/master/gpg.conf

$ cat ~/.gnupg/gpg.conf
personal-cipher-preferences AES256 AES192 AES CAST5
personal-digest-preferences SHA512 SHA384 SHA256 SHA224
default-preference-list SHA512 SHA384 SHA256 SHA224 AES256 AES192 AES CAST5 ZLIB BZIP2 ZIP Uncompressed
cert-digest-algo SHA512
s2k-digest-algo SHA512
s2k-cipher-algo AES256
charset utf-8
fixed-list-mode
no-comments
no-emit-version
keyid-format 0xlong
list-options show-uid-validity
verify-options show-uid-validity
with-fingerprint
require-cross-certification
use-agent

$ chmod 600 ~/.gnupg/gpg.conf
```

## Import Yubikey Public Keys (Widnows and Linux)

For this you will need the .pub file from either USB or net storage. 

```
gpg --import Root.pub
gpg --import MYKEY.pub
gpg --list-keys
```

### Import and Trust root key

Edit the Master key to assign it ultimate trust by selecting `trust` then option `5`:

```
$ gpg --edit-key <Root Key ID>

Secret key is available.

Please decide how far you trust this user to correctly verify other users' keys
(by looking at passports, checking fingerprints from different sources, etc.)

  1 = I don't know or won't say
  2 = I do NOT trust
  3 = I trust marginally
  4 = I trust fully
  5 = I trust ultimately
  m = back to the main menu

Your decision? 5
Do you really want to set this key to ultimate trust? (y/N) y

gpg> save

```
# Encryption and Decryption tests
```
$ echo "test message string" | gpg --encrypt --armor --recipient <youremail> > out.gpg
```

# Decryption
```
$ gpg --decrypt out.gpg
```

# Signing
```
$ echo "test message string" | gpg --armor --clearsign --default-key <youemail>
```

## SSH

[gpg-agent](https://wiki.archlinux.org/index.php/GnuPG#SSH_agent) supports the OpenSSH ssh-agent protocol (`enable-ssh-support`), as well as Putty's Pageant on Windows (`enable-putty-support`). This means it can be used instead of the traditional ssh-agent / pageant. There are some differences from ssh-agent, notably that gpg-agent does not _cache_ keys rather it converts, encrypts and stores them - persistently - as GPG keys and then makes them available to ssh clients. Any existing ssh private keys that you'd like to keep in `gpg-agent` should be deleted after they've been imported to the GPG agent.

When importing the key to `gpg-agent`, you'll be prompted for a passphrase to protect that key within GPG's key store - you may want to use the same passphrase as the original's ssh version. GPG can both cache passphrases for a determined period (ref. `gpg-agent`'s various `cache-ttl` options), and since version 2.1 can store and fetch passphrases via the macOS keychain. Note than when removing the old private key after importing to `gpg-agent`, keep the `.pub` key file around for use in specifying ssh identities (e.g. `ssh -i /path/to/identity.pub`).

Probably the biggest thing missing from `gpg-agent`'s ssh agent support is being able to remove keys. `ssh-add -d/-D` have no effect. Instead, you need to use the `gpg-connect-agent` utility to lookup a key's keygrip, match that with the desired ssh key fingerprint (as an MD5) and then delete that keygrip. The [gnupg-users mailing list](https://lists.gnupg.org/pipermail/gnupg-users/2016-August/056499.html) has more information.

### setup for linux

Create a hardened configuration for gpg-agent with the following options or by downloading my [recommended](https://github.com/drduh/config/blob/master/gpg-agent.conf) version directly:

```
$ curl -Lfo ~/.gnupg/gpg-agent.conf https://raw.githubusercontent.com/drduh/config/master/gpg-agent.conf

$ cat ~/.gnupg/gpg-agent.conf
enable-ssh-support
pinentry-program /usr/bin/pinentry-gnome3
default-cache-ttl 60
max-cache-ttl 120
```
**Note** It is *not* necessary to import the corresponding GPG public key in order to use SSH.

Restart the agent
```
#Restart agent
gpg-connect-agent killagent /bye
gpg-connect-agent /bye
```
Copy and paste the output from `ssh-add` to the server's `authorized_keys` file:

```
$ ssh-add -L
ssh-rsa AAAAB4NzaC1yc2EAAAADAQABAAACAz[...]zreOKM+HwpkHzcy9DQcVG2Nw== cardno:000605553211
```
In the case of YubiKey usage, to extract the public key from the ssh agent:

```
$ ssh-add -L | grep "cardno:000605553211" > ~/.ssh/id_rsa_yubikey.pub
```
Then you can explicitly associate this YubiKey-stored key for used with a host, `github.com` for example, as follows:

```
$ cat << EOF >> ~/.ssh/config
Host bitbucket.aadtest.ca
    Port 7999   <-- If you put this you can omit the port below, Github doesnt need this
    IdentitiesOnly yes
    IdentityFile ~/.ssh/id_rsa_yubikey.pub
EOF
```

**note** If it doesnt pick up the card in linux, check that:

 ```SSH_AUTH_SOCK=<Path to GNUPG>/S.gpg-agent.ssh```

## Replace agents

To launch `gpg-agent` for use by SSH, use the `gpg-connect-agent /bye` or `gpgconf --launch gpg-agent` commands.

Add these to your shell `rc` file:

```
export GPG_TTY="$(tty)"
export SSH_AUTH_SOCK=$(gpgconf --list-dirs agent-ssh-socket)
gpgconf --launch gpg-agent
```

### Connect with public key authentication
```
$ $ ssh git@bitbucket.aadtest.ca:7999 -vvv
[...]
OpenSSH_7.2p2 Ubuntu-4ubuntu2.6, OpenSSL 1.0.2g  1 Mar 2016
debug1: Reading configuration data /home/simon/.ssh/config
debug1: Reading configuration data /etc/ssh/ssh_config
debug1: /etc/ssh/ssh_config line 19: Applying options for *
debug2: resolving "bitbucket.aadtest.ca:7999" port 22
ssh: Could not resolve hostname bitbucket.aadtest.ca:7999: Name or service not known
simon@simon-pc:~/Downloads/code/test-code$ man ssh
simon@simon-pc:~/Downloads/code/test-code$ ssh -p 7999 git@bitbucket.aadtest.ca -vvv
OpenSSH_7.2p2 Ubuntu-4ubuntu2.6, OpenSSL 1.0.2g  1 Mar 2016
debug1: Reading configuration data /home/simon/.ssh/config
debug1: /home/simon/.ssh/config line 1: Applying options for bitbucket.aadtest.ca
debug1: Reading configuration data /etc/ssh/ssh_config
debug1: /etc/ssh/ssh_config line 19: Applying options for *
debug2: resolving "bitbucket.aadtest.ca" port 7999
debug2: ssh_connect_direct: needpriv 0
debug1: Connecting to bitbucket.aadtest.ca [40.86.225.116] port 7999.
debug1: Connection established.
debug1: identity file /home/simon/.ssh/id_rsa_yubikey.pub type 1
debug1: Authentication succeeded (publickey).
[...]
```
## Windows

- Export the SSH key from GPG: `> gpg --export-ssh-key <your public key's id>`

Copy this key to a file for later use. It represents the public SSH key corresponding to the secret key on your YubiKey. You can upload this key to any server you wish to SSH into.

- Create a shortcut that points to `gpg-connect-agent /bye` and place it in your startup folder `shell:startup` to make sure the agent starts after a system shutdown. Modify the shortcut properties so it starts in a "Minimized" window, to avoid unnecessary noise at startup.

Now you can use PuTTY for public key SSH authentication. When the server asks for public key verification, PuTTY will forward the request to GPG, which will prompt you for your PIN and authorize the login using your YubiKey.

## Git

You can use YubiKey to sign GitHub commits and tags. It can also be used for GitHub SSH authentication, allowing you to push, pull, and commit without a password.

Login to GitHub and upload SSH and PGP public keys in Settings.

To configure a signing key:

	> git config --global user.signingkey <SIGNATURE SUBKEY ID> e.g (EF876E47 for jsmith@aadtest.ca)

Make sure the user.email option matches the email address associated with the PGP identity.

Now, to sign commits or tags simply use the `-S` option. GPG will automatically query your YubiKey and prompt you for your PIN.

You might also need to point Git to use gpg2 and always gpgsign

    > git config --global gpg.program gpg2
    > git config --global gpg.program "C:\Program Files (x86)\GnuPG\bin\gpg.exe" <- In Windows

Forgot to sign a git commit?

```
git rebase --exec 'git commit --amend --no-edit -n -S' -i <commit number you want to stop at>
```

To authenticate with SSH:

**Windows** Run the following command for Bitbucket:

	> git config --global core.sshcommand 'plink -P 7999 -agent'

The port (-P 7999) matters for Git-SCM as it cannot pass the port via <user@host:port> as expected when using plink. Plink is required as putty is the only ssh client that supports ssh cards.

**Note** If you encounter the error `gpg: signing failed: No secret key` - run `gpg --card-status` with YubiKey plugged in and try the git command again.

## VMWare

From [this](https://support.yubico.com/support/solutions/articles/15000008891-troubleshooting-vmware-workstation-device-passthrough) guide you will need to modify the .vmx file to ensure the Yubikey is **not** shared between the host and the guest.

```
usb.generic.allowHID = "TRUE"
usb.generic.allowLastHID = "TRUE"
```
Then select Removable Devices -> Yubico YubiKey 4 OTP+U2F+CCID. You should not see the work "Shared". Yes this means you need to move the key back and forth..... sucks, but this way there are not keys lying around in every different vm guest...


# Administration and KEY creation

## Entropy in Linux

Generating keys will require a lot of randomness. To check the available bits of entropy available on Linux:

```
$ cat /proc/sys/kernel/random/entropy_avail
849 

$ sudo apt-get install -y rng-tools 
$ sudo service rng-tools restart

$ cat /proc/sys/kernel/random/entropy_avail
3101
```

An entropy pool value greater than 3000 is sufficient.

## Creating keys

Its not a good idea to dump private keys all over your personal box and then move it into a smart card. If your going to be serious about things you need to provision key material on a standalone device, yup sux... - Anyway if your not going to listen here is the next best thing....

Create a temporary directory which will be deleted on [reboot](https://en.wikipedia.org/wiki/Tmpfs). This will be used for the temp gpg keyring being created. 

```
$ export GNUPGHOME=$(mktemp -d) ; echo $GNUPGHOME
/tmp/tmp.aaiTTovYgo
```

Create a hardened configuration for GPG with the following options 

```
$ cat $GNUPGHOME/gpg.conf
personal-cipher-preferences AES256 AES192 AES
personal-digest-preferences SHA512 SHA384 SHA256
personal-compress-preferences ZLIB BZIP2 ZIP Uncompressed
default-preference-list SHA512 SHA384 SHA256 AES256 AES192 AES ZLIB BZIP2 ZIP Uncompressed
cert-digest-algo SHA512
s2k-digest-algo SHA512
s2k-cipher-algo AES256
charset utf-8
fixed-list-mode
no-comments
no-emit-version
keyid-format 0xlong
list-options show-uid-validity
verify-options show-uid-validity
with-fingerprint
require-cross-certification
throw-keyids
use-agent
```
Had to remove: ```no-symkey-cache, no-tty```

## Generate Root of Trust Key

This will be used later to sign all the User Keys. Without it there is no explicit trust between git commit signing keys as they can be 
arbitrarily created and uploaded to BitBucket/GutHub. By signing the User Public Keys from a "Trusted Root" any signature can be implicitly 
trusted. 

For git this means that users only have to assign "ultimte trust" to the root key, which signs all the user keys and then imported user keys will automatically be trusted and dont need to be verified.

```
#!/bin/bash

#Usage $ genRoot.sh <name>@domain

pass=$(gpg --gen-random -a 0 24)

echo Copy this password
echo $pass
read

cat >foo <<EOF
     %echo Generating a basic OpenPGP key
     Key-Type: RSA
     Key-Usage: sign	
     Key-Length: 3072
     Subkey-Type: RSA
     Subkey-Length: 3072
     Subkey-Usage: sign
     Name-Real: Root Trust Key
     Name-Email: ${1}
     Expire-Date: 0
     %ask-passphrase
     # Do a commit here, so that we can later print "done" :-)
     %commit
     %echo done
EOF

gpg2 --batch --gen-key foo
rm foo              

echo "Enter Key Name Just Created"
read key

echo Copy this password, then hit Enter
echo $pass
read

gpg2 --armor --export-secret-keys ${key} > $GNUPGHOME/${key}_master.key
gpg2 --armor --export-secret-subkeys ${key} > $GNUPGHOME/${key}_sub.key
gpg2 --armor --export ${key} > $GNUPGHOME/${key}.pub
```
Make sure to write down the Root Key password somewhere

## User Master Key

This is the minimum recommendation from [CSE](https://cyber.gc.ca/en/guidance/cryptographic-algorithms-unclassified-protected-and-protected-b-information-itsp40111), with 3072 as of 2030.

The following script can automate the process from [Github](https://github.com/drduh/YubiKey-Guide/)

```
#!/bin/bash

pass=$(gpg --gen-random -a 0 24)

#Usage $ genKey.sh <Real Name> <Email>

echo Copy this password, then hit Enter
echo $pass
read

cat >foo <<EOF
     %echo Generating a basic OpenPGP key
     Key-Type: RSA
     Key-Usage: sign	
     Key-Length: 2048
     Name-Real: ${1}
     Name-Email: ${2}
     Expire-Date: 10y
     %ask-passphrase
     # Do a commit here, so that we can later print "done" :-)
     %commit
     %echo done
EOF

gpg2 --batch --gen-key foo
rm foo

echo "Enter Key Name Just Created"
read key

echo Copy this password, then Hit Enter
echo $pass
read

{
    echo addkey
    echo 4     # RSA (sign only)
    echo 2048  # key length
    echo 3y    # key expire
    echo save
} | gpg2 --expert --command-fd=0 --status-fd=1 --edit-key ${key}
{
    echo addkey
    echo 6     # RSA (Encrypt)
    echo 2048  # key length
    echo 3y    # key expire
    echo save
} | gpg2 --expert --command-fd=0 --status-fd=1 --edit-key ${key}
{
    echo addkey
    echo 8     # RSA (Authentication)
    echo s
    echo e
    echo a
    echo q
    echo 2048  # key length
    echo 3y    # key expire
    echo save
} | gpg2 --expert --command-fd=0 --status-fd=1 --edit-key ${key}

gpg2 --list-secret-keys

echo "Enter Root Signing Key ID: "
read root

{
    echo 3     # I have done very careful checking
    echo y     # Really Sign
} | gpg2 --command-fd=0 --status-fd=1 --ask-cert-level --sign-with ${root} --sign-key ${2}

echo Copy this password, then Hit Enter
echo $pass
read

gpg2 --armor --export-secret-keys ${key} > $GNUPGHOME/${key}_master.key
gpg2 --armor --export-secret-subkeys ${key} > $GNUPGHOME/${key}_sub.key
gpg2 --armor --export ${key} > $GNUPGHOME/${key}_${2}.pub
```
## Configure Smartcard

Use GPG to configure YubiKey as a smartcard: Verify the "OTP U2F CCID" modes

```
$ gpg --card-edit
Reader ...........: Yubico Yubikey 4 OTP U2F CCID 00 00
Application ID ...: D2760001240102010006087189620000
Version ..........: 2.1
Manufacturer .....: Yubico
Serial number ....: 08718962
Name of cardholder: [not set]
Language prefs ...: [not set]
Sex ..............: unspecified
URL of public key : [not set]
Login data .......: [not set]
Signature PIN ....: forced
Key attributes ...: rsa2048 rsa2048 rsa2048
Max. PIN lengths .: 0 0 0
PIN retry counter : 0 0 0
Signature counter : 0
Signature key ....: [none]
Encryption key....: [none]
Authentication key: [none]
General key info..: [none]
```
## Change PIN

The default PIN is `123456` and default Admin PIN (PUK) is `12345678`. CCID-mode PINs can be up to 127 ASCII characters long.

The Admin PIN is required for some card operations and to unblock a PIN that has been entered incorrectly more than three times. See the GnuPG documentation on [Managing PINs](https://www.gnupg.org/howtos/card-howto/en/ch03s02.html) for details.

If you get errors, quit and remove/re-insert the card.

```
gpg/card> admin
Admin commands are allowed

gpg/card> passwd
gpg: OpenPGP card no. D2760001240102010006055532110000 detected

1 - change PIN
2 - unblock PIN
3 - change Admin PIN
4 - set the Reset Code
Q - quit

Your selection? 3
PIN changed.

1 - change PIN
2 - unblock PIN
3 - change Admin PIN
4 - set the Reset Code
Q - quit

Your selection? 1
PIN changed.

1 - change PIN
2 - unblock PIN
3 - change Admin PIN
4 - set the Reset Code
Q - quit

Your selection? q
```
## Set information

Some fields are optional.

```
$ gpg2 --card-edit 

Reader ...........: Yubico Yubikey 4 OTP U2F CCID 00 00
Application ID ...: D2760001240102010006087189620000
Version ..........: 2.1
Manufacturer .....: Yubico
Serial number ....: 08718962
Name of cardholder: [not set]
Language prefs ...: [not set]
Sex ..............: unspecified
URL of public key : [not set]
Login data .......: [not set]
Signature PIN ....: not forced
Key attributes ...: rsa2048 rsa2048 rsa2048
Max. PIN lengths .: 127 127 127
PIN retry counter : 3 0 3
Signature counter : 0
Signature key ....: [none]
Encryption key....: [none]
Authentication key: [none]
General key info..: [none]

gpg/card> admin
Admin commands are allowed

gpg/card> name
Cardholder's surname: Smith
Cardholder's given name: John

gpg/card> lang
Language preferences: en

gpg/card> login
Login data (account name): jsmith@test.com

gpg/card> 

Reader ...........: Yubico Yubikey 4 OTP U2F CCID 00 00
Application ID ...: D2760001240102010006087189620000
Version ..........: 2.1
Manufacturer .....: Yubico
Serial number ....: 08718962
Name of cardholder: John Smith
Language prefs ...: en
Sex ..............: unspecified
URL of public key : [not set]
Login data .......: jsmith@test.com
Signature PIN ....: not forced
Key attributes ...: rsa2048 rsa2048 rsa2048
Max. PIN lengths .: 127 127 127
PIN retry counter : 3 0 3
Signature counter : 0
Signature key ....: [none]
Encryption key....: [none]
Authentication key: [none]
General key info..: [none]

gpg/card> 
```
## Transfer keys

**Important** Transferring keys to YubiKey using `keytocard` is a destructive, one-way operation only. Make sure you've made a backup before proceeding: `keytocard` converts the local, on-disk key into a stub, which means the on-disk copy is no longer usable to transfer to subsequent security key devices or mint additional keys.

Previous GPG versions required the `toggle` command before selecting keys. The currently selected key(s) are indicated with an `*`. When moving keys only one key should be selected at a time.

IMPORTANT: The Master (e.g SC) key will not be stored on the yubikey. This is because the SC is used to sign/revoke subkeys. It should be stored offline and only used for recovery (i.e lost yubikeys).

```
$ gpg2 --edit-key $KEYID

gpg (GnuPG) 2.1.11; Copyright (C) 2016 Free Software Foundation, Inc.
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.

Secret key is available.

sec  rsa2048/0xF16FF5F060C0C1CF
     created: 2019-01-05  expires: 2029-01-02  usage: SC  
     trust: ultimate      validity: ultimate
ssb  rsa2048/0xA8BAEE0FAD1AA49A
     created: 2019-01-05  expires: 2022-01-04  usage: S   
ssb  rsa2048/0x986B0BE710FA208E
     created: 2019-01-05  expires: 2022-01-04  usage: E   
ssb  rsa2048/0xE2785DDC946FEB3E
     created: 2019-01-05  expires: 2022-01-04  usage: A   
[ultimate] (1). John Smith <jsmith@test.com>
```

## Signing

Select and move the signature key. You will be prompted for the key passphrase and Admin PIN.

```
gpg> key 1

gpg> keytocard
Please select where to store the key:
   (1) Signature key
   (3) Authentication key
Your selection? 1
```

## Encryption

Type `key 1` again to de-select and `key 2` to select the next key:

```
gpg> key 1

gpg> key 2

gpg> keytocard
Please select where to store the key:
   (2) Encryption key
Your selection? 2

[...]
```
## Authentication

Type `key 2` again to deselect and `key 3` to select the last key:

```
gpg> key 2

gpg> key 3

gpg> keytocard
Please select where to store the key:
   (3) Authentication key
Your selection? 3

gpg> save
```

## Recover Keys from Backup

insert new yubikey and verify status

```
$ gpg2 --card-status
gpg: keybox '/tmp/tmp.fiQ9NRnTqX/pubring.kbx' created
Reader ...........: Yubico Yubikey 4 OTP U2F CCID 00 00
Application ID ...: D2760001240102010006087188080000
Version ..........: 2.1
Manufacturer .....: Yubico
Serial number ....: 08718808
Name of cardholder: [not set]
Language prefs ...: [not set]
Sex ..............: unspecified
URL of public key : [not set]
Login data .......: [not set]
Signature PIN ....: not forced
Key attributes ...: rsa2048 rsa2048 rsa2048
Max. PIN lengths .: 127 127 127
PIN retry counter : 3 0 3
Signature counter : 0
Signature key ....: [none]
Encryption key....: [none]
Authentication key: [none]
General key info..: [none]
```

Here we have new Yubikey that we are going to restore to.

Import in the backup

```
$ gpg2 --import ~/Downloads/pkitest/440A8572B9C5B5C4_jsmith@aadtest.ca.pub 
gpg: key 0x440A8572B9C5B5C4: public key "John Smith <jsmith@aadtest.ca>" imported
gpg: Total number processed: 1
gpg:               imported: 1
gpg: no ultimately trusted keys found

$ gpg2 --import ~/Downloads/pkitest/372865F26FD44F86.pub 
gpg: key 0x372865F26FD44F86: public key "Root Trust Key <root@aadtest.ca>" imported
gpg: Total number processed: 1
gpg:               imported: 1

$ gpg2 --import ~/Downloads/pkitest/440A8572B9C5B5C4_sub.key 
gpg: key 0x440A8572B9C5B5C4: "John Smith <jsmith@aadtest.ca>" not changed
gpg: To migrate 'secring.gpg', with each smartcard, run: gpg --card-status
gpg: key 0x440A8572B9C5B5C4: secret key imported
gpg: Total number processed: 5
gpg:              unchanged: 1
gpg:       secret keys read: 5
gpg:   secret keys imported: 3
```

re-import to the smart card from [Transfer-Keys](## Transfer keys)
    - If you get a "General Error" - re-insert the key
If you get errors requesting a key with a different serial number be inserted try:
    - deleting the keys in the key ring and re-adding them, then changing the SSH carno: setting
    - backup the .conf files in .gnupg or %APPDATA and re-create (re-install)

It looks like there are many serial numebr associations that exists so swaping physical keys is non-trival

## Troubleshooting

See [This](https://github.com/drduh/YubiKey-Guide#troubleshooting) guide

## Reseting the GPG Applet

See [This](https://support.yubico.com/support/solutions/articles/15000006421-resetting-the-openpgp-applet-on-the-yubikey)
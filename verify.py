from shutil import rmtree
import gnupg
import requests
import subprocess
import os
import sys
import getopt


def importGPGKey(key, gpgObj, Trust):
    # import and trust
    result = gpgObj.import_keys(key)
    assert (result.count > 0), 'Could not read in key'

    if Trust: # only root keys are explicitly Trusted
        gpgObj.trust_keys(result.fingerprints, 'TRUST_ULTIMATE')


def getGPGKeys(user, gpgObj):

    url = 'https://api.github.com/users/'

    try:
        r = requests.get(url + user + '/gpg_keys') # https://developer.github.com/v3/users/gpg_keys/

        if r.status_code == 200:
            if 0 < len(r.json()):
                for key in r.json():  # users can have multiple keys
                    importGPGKey(key['raw_key'], gpgObj, False)  # Trust must be implicit to an existing Root
        else:
            raise ConnectionError

    except ConnectionError:
        print("Couldn't retrieve user key")
    except TimeoutError:
        print("Couldn't connect to API")


def validateCommitHash(commitHash, trustDir):

    gitENV              = os.environ.copy()
    gitENV["GNUPGHOME"] = trustDir
    gitVerify           = "git verify-commit --raw"
    gitCommand          = gitVerify + " " + commitHash
    sigExists           = False
    sigValid            = False
    sigTrusted          = False

    with subprocess.Popen(gitCommand, shell=True, stderr=subprocess.PIPE, stdout=subprocess.PIPE,
                                      env=gitENV) as gitVerifyOutput:
        for validation in gitVerifyOutput.stderr:

            validationStr = validation.decode('utf-8')

            # Does the Signature exist
            if 'NEWSIG' in validationStr:
                sigExists = True
            # Does the Signature validate from gpg
            elif 'GOODSIG' in validationStr:
                sigValid = True
            # Does the signature chain to a valid Trust point (i.e Root KeyPair)
            elif 'TRUST_ULTIMATE' in validationStr: # Root keys
                sigTrusted = True
            elif  'TRUST_FULLY' in validationStr: # Chained User Keys
                sigTrusted = True

    return sigValid, sigTrusted, sigExists


def validateCommitHashes(trustDir, gpgObj, whitelist):

    gitLogGen = "git log --format=\"%H %cN %ce\""
    blackList = [ 'simon' ] # usernames in here wont be tried again
    validHash = False

    with subprocess.Popen(gitLogGen, shell=True, stdout=subprocess.PIPE) as gitLogOutput:
        for line in gitLogOutput.stdout:

            commitDetails = line.decode('utf-8').split()
            commitHash    = commitDetails[0]
            userName      = commitDetails[1]
            userEmail     = commitDetails[2]

            # check if commit hash was whitelisted
            if commitHash not in whitelist:
                # If no signature exists, just report
                if (False, False, False) == validateCommitHash(commitHash, trustDir):
                    print(commitHash + " no signature exists from " + userName + " " + userEmail)  # Bad Key

                # If a non-valid sig exists try get the public key
                elif (False, False, True) == validateCommitHash(commitHash, trustDir):
                    if userName not in blackList: # If we failed at this before, don't waste Git API calls
                        getGPGKeys(userName, gpgObj)  # Try fetch the key from GitGub
                        # Good now?
                        if (True, True, True) != validateCommitHash(commitHash, trustDir):
                            blackList.append(userName)  # Add user to blacklist
                            print(commitHash + " is not valid from " + userName + " " + userEmail)  # Bad Key

                # If we dont have a valid, trusted signature
                elif (True, True, True) != validateCommitHash(commitHash, trustDir):
                    print(commitHash + " is not valid from " + userName + " " + userEmail)  # Bad Key


def checkFileModDates(expectedModDatesDict):

    gitLogGen = "git ls-tree -r --name-only HEAD | while read filename; \
                 do echo \"$(git log -1 --format=\"%ct\" \
                 -- $filename) $filename\"; done"

    with subprocess.Popen(gitLogGen, shell=True, stdout=subprocess.PIPE) as gitLogOutput:
        for line in gitLogOutput.stdout:

            gitLogLine = line.decode('utf-8').split()
            utcDate    = gitLogLine[0]
            gitDir     = gitLogLine[1] + '\n'  # to match expected

            # check if matched dir has the expected mod date
            if gitDir in expectedModDatesDict:
                # if the current utc date is newer this is unexpected
                if expectedModDatesDict[gitDir] != utcDate:
                    print("ERROR: file has been unexpectedly modified %s ", gitDir)


def checkGitIntegrity():

    gitfsck = "git fsck"

    with subprocess.Popen(gitfsck, shell=True, stdout=subprocess.PIPE) as gitOutput:
        for line in gitOutput.stdout:
            line = line.decode('utf-8')

            errorCode = ['error', 'invalid', 'fatal']
            for i in errorCode:
                if line.__contains__(i):
                    print("ERROR: invalid integrity")


def buildTrustDB(trustDir, keyDir, reuse):

    if not reuse:
        if os.path.exists(trustDir):
            rmtree(trustDir) # clean previous db
        os.mkdir(trustDir)

    gpgObj          = gnupg.GPG(gnupghome=trustDir)
    gpgObj.encoding = 'utf-8'

    # Read in trusted roots from dir
    for path, dir, files in os.walk(keyDir):
        for file in files:
            if file.endswith('pub'):  # only public key files
                with open(os.path.join(path,file)) as keyFile:
                    key = keyFile.read()
                importGPGKey(key, gpgObj, True)

    return gpgObj


def buildDict(file, fileDict, reverse=False, delim=','):

    status = False

    if os.path.isfile(file):
        with open(file) as fileList:
            for line in fileList:
                if line != '\n': # ignore empty line
                    first, second = line.split(delim)
                    if not reverse:
                        fileDict[first] = second
                    else:
                        fileDict[second] = first

        status = True

    return status


def usage():

    print("Usage:\n\
          --db <existing db dir>\n\
          --key <existing key dir>\n\
          --whitelist <file>\n\
          --tripwire <file>\n")


def main(argv):

    # Defaults
    trustDir = './trust'
    keyDir = './keys'
    whiteListFile = './whitelist'
    tripWireFile = './tripwire'

    whiteListDict = {}
    modDateCheckDict = {}
    reuse = False

    try:
        opts, args = getopt.getopt(argv, 'h:', ['db=', 'key=', 'whitelist=', 'tripwire='])
    except getopt.GetoptError:
        usage()
        sys.exit(1)

    for opt, arg in opts:
        if opt == '-h':
            usage()
            sys.exit()
        elif opt == '--db':
            trustDir = arg
            reuse = True
        elif opt == '--key':
            keyDir = arg
        elif opt == '--whitelist':
            whiteListFile = arg
        elif opt == '--tripwire':
            tripWireFile = arg

    # Check that commit signatures chain to expected root
    if buildDict(whiteListFile, whiteListDict):
        gpgObj = buildTrustDB(trustDir, keyDir, reuse)
        validateCommitHashes(trustDir, gpgObj, whiteListDict)

    # Check that expected files haven't changed
    if buildDict(tripWireFile, modDateCheckDict, reverse=True, delim=' '):
        checkFileModDates(modDateCheckDict)

    # Check git integrity of the repo
    checkGitIntegrity()

if __name__ == "__main__":
    main(sys.argv[1:])

